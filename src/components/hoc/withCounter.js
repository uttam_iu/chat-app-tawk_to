import React from 'react'
export function withCounter(OriginalComponent) {

    // //**** with functional comp ****

    return function NewComp() {
        const [count, setCount] = React.useState(0);

        const handleCounter = (increment) => {
            if (increment) setCount(count + 1);
            else setCount(count + 1);
        };

        return <OriginalComponent count={count} handleCounter={handleCounter} />;
    }


    // //**** with class comp ****

    // return class NewComp extends React.Component {
    //     state = { count: 0 };

    //     handleCounter = (increment) => {
    //         if (increment) this.setState({ count: (this.state.count + 1) });
    //         else this.setState({ count: (this.state.count - 1) });
    //     };

    //     render() {
    //         return <OriginalComponent count={this.state.count} handleCounter={this.handleCounter} />;
    //     }
    // }
};
