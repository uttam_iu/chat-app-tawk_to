import React from 'react';

const ComponentB = props => {
    const { count, handleCounter } = props;

    console.log('ComponentB');

    return (
        <div>
            <button type='button' onClick={() => handleCounter(true)}>increase</button>
            <button type='button' onClick={() => handleCounter(false)}>decrease</button>
            <p>{`this is ComponentB & counter = ${count}`}</p>
        </div>
    );
};

export default React.memo(ComponentB);
