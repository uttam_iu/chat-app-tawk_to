import React from 'react';
import { withCounter } from './hoc/withCounter';

const ComponentA = props => {
    const { count, handleCounter } = props;

    console.log('ComponentA');

    return (
        <div>
            <button type='button' onClick={() => handleCounter(true)}>increase</button>
            <button type='button' onClick={() => handleCounter(false)}>decrease</button>
            <p>{`this is ComponentA & counter = ${count}`}</p>
        </div>
    );
};

export default React.memo(withCounter(ComponentA));
