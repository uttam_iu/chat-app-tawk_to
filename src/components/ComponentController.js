import React from 'react';
import ComponentA from './ComponentA';
import ComponentB from './ComponentB';
import ComponentC from './ComponentC';

const ComponentController = props => {
    const [countB, setCountB] = React.useState(0);
    const [countC, setCountC] = React.useState(0);


    const handleCounterB = React.useCallback((increment) => {
        if (increment) setCountB(countB + 1);
        else setCountB(countB - 1);
    }, [countB]);

    const handleCounterC = React.useCallback((increment) => {
        if (increment) setCountC(countC + 1);
        else setCountC(countC - 1);
    }, [countC]);

    return (
        <div>
            <ComponentA />
            <ComponentB count={countB} handleCounter={handleCounterB} />
            <ComponentC count={countC} handleCounter={handleCounterC} />
        </div>
    );
};

export default ComponentController;
