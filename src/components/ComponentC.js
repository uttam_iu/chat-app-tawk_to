import React from 'react';

const ComponentC = props => {
    const { count, handleCounter } = props;

    console.log('ComponentC');

    return (
        <div>
            <button type='button' onClick={() => handleCounter(true)}>increase</button>
            <button type='button' onClick={() => handleCounter(false)}>decrease</button>
            <p>{`this is ComponentC & counter = ${count}`}</p>
        </div>
    );
};

export default React.memo(ComponentC);
