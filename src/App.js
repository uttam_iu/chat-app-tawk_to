import React from 'react';
import ComponentController from './components/ComponentController';

// let innerDoc = [], attentionTextElem = [];

const App = () => {
	// let brndTmInt = React.useRef(null);
	// let docTmInt = React.useRef(null);
	// let menuTmInt = React.useRef(null);
	// let attTmInt = React.useRef(null);

	// //getting inner document for chatting iframe
	// const getInnerDocument = React.useCallback(() => {
	// 	docTmInt.current = setInterval(() => {
	// 		const ifr = document.getElementsByTagName('iframe');
	// 		if (ifr) {
	// 			let ind = 0, atInd = 0;
	// 			for (let index = 0; index < ifr.length; index++) {
	// 				const element = ifr[index];
	// 				const doc = element.contentDocument || element.contentWindow.document;
	// 				const btnElem = doc.getElementsByClassName('tawk-button tawk-button-circle');
	// 				if (btnElem) {
	// 					innerDoc[ind] = doc;
	// 					ind++;
	// 				}

	// 				const atElm = doc.getElementsByClassName('tawk-icon-right');
	// 				if (atElm) {
	// 					attentionTextElem[atInd] = atElm;
	// 					atInd++;
	// 				}
	// 			}
	// 		}
	// 	}, 50);
	// }, []);

	// //removing branding
	// const removeBranding = React.useCallback(() => {
	// 	brndTmInt.current = setInterval(() => {
	// 		if (innerDoc.length > 0) {
	// 			for (let i = 0; i < innerDoc.length; i++) {
	// 				const branding = innerDoc[i].getElementsByClassName('tawk-branding');
	// 				if (branding) {
	// 					for (let index = 0; index < branding.length; index++) {
	// 						branding[index].style.display = 'none';
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}, 50);
	// }, []);

	// //removing pop out widget
	// const removePopUpFromMenu = React.useCallback(() => {
	// 	menuTmInt.current = setInterval(() => {
	// 		if (innerDoc.length > 0) {
	// 			for (let i = 0; i < innerDoc.length; i++) {
	// 				const menuItms = innerDoc[i].getElementsByClassName('tawk-dropdown-menu tawk-open tawk-dropdown-menu-right');
	// 				if (menuItms && menuItms[0]) {
	// 					const menuBtns = menuItms[0].getElementsByTagName('button');
	// 					if (menuBtns) {
	// 						for (let index = 0; index < menuBtns.length; index++) {
	// 							const popUpMenu = menuBtns[index];
	// 							const text = popUpMenu.innerText.trim();
	// 							if (text === 'Pop out widget') {
	// 								popUpMenu.style.visibility = 'hidden';
	// 								popUpMenu.style.height = '0px';
	// 								popUpMenu.style.padding = '0px';
	// 							}
	// 						}
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}, 50);
	// }, []);

	// //removing attention text
	// const removingAttentionText = React.useCallback(() => {
	// 	attTmInt.current = setInterval(() => {
	// 		if (attentionTextElem.length > 0)
	// 			for (let i = 0; i < attentionTextElem.length; i++)
	// 				attentionTextElem[i].style.display = 'none';
	// 	}, 50);
	// }, []);

	// //effect
	// React.useEffect(() => {
	// 	getInnerDocument();
	// 	removeBranding();
	// 	removePopUpFromMenu();
	// 	// removingAttentionText();
	// 	return () => {
	// 		if (brndTmInt && brndTmInt.current) clearInterval(brndTmInt.current);
	// 		if (docTmInt && docTmInt.current) clearInterval(docTmInt.current);
	// 		if (menuTmInt && menuTmInt.current) clearInterval(menuTmInt.current);
	// 		if (attTmInt && attTmInt.current) clearInterval(attTmInt.current);
	// 	}
	// }, [removeBranding, getInnerDocument, removePopUpFromMenu, removingAttentionText, brndTmInt, docTmInt, menuTmInt, attTmInt]);

	return (
		<div className="App">
			hello, chatting service is from tawk.to
			{/* <ComponentController /> */}
		</div>
	);
};

export default App;
