//tawk.to script
var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
(function () {
	var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
	s1.async = true;
	s1.src = 'https://embed.tawk.to/61ac4bf3c82c976b71bfb051/1fm4ghbv6';
	s1.charset = 'UTF-8';
	s1.setAttribute('crossorigin', '*');
	s0.parentNode.insertBefore(s1, s0);
})();


//for hiding tawk.io logo
let innerDoc = [], attentionTextElem = [];

var brndTmInt = null;
var docTmInt = null;
var menuTmInt = null;
var attTmInt = null;

//getting inner document for chatting iframe
const getInnerDocument = () => {
	docTmInt = setInterval(() => {
		const ifr = document.getElementsByTagName('iframe');
		if (ifr) {
			let ind = 0, atInd = 0;
			for (let index = 0; index < ifr.length; index++) {
				const element = ifr[index];
				const doc = element.contentDocument || element.contentWindow.document;
				const btnElem = doc.getElementsByClassName('tawk-button tawk-button-circle');
				if (btnElem) {
					innerDoc[ind] = doc;
					ind++;
				}

				const atElm = doc.getElementsByClassName('tawk-icon-right');
				if (atElm) {
					attentionTextElem[atInd] = atElm;
					atInd++;
				}
			}
		}
	}, 50);
};

//removing branding
const removeBranding = () => {
	brndTmInt = setInterval(() => {
		if (innerDoc.length > 0) {
			for (let i = 0; i < innerDoc.length; i++) {
				const branding = innerDoc[i].getElementsByClassName('tawk-branding');
				if (branding) {
					for (let index = 0; index < branding.length; index++) {
						branding[index].style.display = 'none';
					}
				}
			}
		}
	}, 50);
};

//removing pop out widget
const removePopUpFromMenu = () => {
	menuTmInt = setInterval(() => {
		if (innerDoc.length > 0) {
			for (let i = 0; i < innerDoc.length; i++) {
				const menuItms = innerDoc[i].getElementsByClassName('tawk-dropdown-menu tawk-open tawk-dropdown-menu-right');
				if (menuItms && menuItms[0]) {
					const menuBtns = menuItms[0].getElementsByTagName('button');
					if (menuBtns) {
						for (let index = 0; index < menuBtns.length; index++) {
							const popUpMenu = menuBtns[index];
							const text = popUpMenu.innerText.trim();
							if (text === 'Pop out widget') {
								popUpMenu.style.visibility = 'hidden';
								popUpMenu.style.height = '0px';
								popUpMenu.style.padding = '0px';
							}
						}
					}
				}
			}
		}
	}, 50);
};

//removing attention text
const removingAttentionText = () => {
	attTmInt = setInterval(() => {
		if (attentionTextElem.length > 0)
			for (let i = 0; i < attentionTextElem.length; i++) {
				if (attentionTextElem[i] && attentionTextElem[i].style)
					attentionTextElem[i].style.display = 'none';
			}
	}, 50);
};

getInnerDocument();
removeBranding();
removePopUpFromMenu();
removingAttentionText();
